include std/util/vsort.e
include etc/wildcard.e

-- player inventory db:
-- <a href="itemref://123456/123456/1">Some Item</a>
-- {name,loref,hiref,ql,timestamp,guild,price,owner,category}

-- with trace

constant MAX_RESULTS = 50

constant TIMER_UPDATE_INVENTORY = new_timer()
constant TIMEOUT_UPDATE_INVENTORY = 60*1000 -- 1 minute
integer timeout_update_inventory_rID      timeout_update_inventory_rID = EOF

integer update    update = FALSE

constant
      iUID  = enum_start(1,1)
     ,iLREF = enum()
     ,iHREF = enum()
     ,iQL   = enum()
     ,iNAME = enum()
     ,iOWNER = enum()
     ,iCATEGORY = enum()
     ,iGUILD = enum()
     ,iPRICE = enum()
     ,iCOMMENT = enum()
     ,iSTAMP = enum()

sequence inventory

procedure load_inventory()
 sequence tmp
 integer fn,len
  inventory = repeat({},iSTAMP)
  output("Loading Inventory DB...",{})
  if length(dir("db/inventory/uid.dat")) then
    allow_error_trapping(OFF)
    fn = fopen("db/inventory/uid.dat","rb")
      len = floor(fsize(fn)/4)
      inventory[iUID] = get4u({fn,len})
    fclose(fn)
    inventory[iLREF] = repeat(NULL,len)
    if length(dir("db/inventory/lref.dat")) then
      fn = fopen("db/inventory/lref.dat","rb")      inventory[iLREF]      = get4u({fn,len})       fclose(fn)
    end if
    inventory[iHREF] = repeat(NULL,len)
    if length(dir("db/inventory/href.dat")) then
      fn = fopen("db/inventory/href.dat","rb")      inventory[iHREF]      = get4u({fn,len})       fclose(fn)
    end if
    inventory[iQL] = repeat(NULL,len)
    if length(dir("db/inventory/ql.dat")) then
      fn = fopen("db/inventory/ql.dat","rb")        inventory[iQL]        = get4u({fn,len})       fclose(fn)
    end if
    inventory[iNAME] = repeat({},len)
    if length(dir("db/inventory/name.dat")) then
      fn = fopen("db/inventory/name.dat","rb")      inventory[iNAME]      = get_string({fn,len})  fclose(fn)
    end if
    inventory[iOWNER] = repeat({},len)
    if length(dir("db/inventory/owner.dat")) then
      fn = fopen("db/inventory/owner.dat","rb")     inventory[iOWNER]     = get_string({fn,len})  fclose(fn)
    end if
    inventory[iCATEGORY] = repeat({},len)
    if length(dir("db/inventory/category.dat")) then
      fn = fopen("db/inventory/category.dat","rb")  inventory[iCATEGORY]  = get_string({fn,len})  fclose(fn)
    end if
    inventory[iGUILD] = repeat(NULL,len)
    if length(dir("db/inventory/guild.dat")) then
      fn = fopen("db/inventory/guild.dat","rb")     inventory[iGUILD]     = getn(fn,len)          fclose(fn)
    end if
    inventory[iPRICE] = repeat(NULL,len)
    if length(dir("db/inventory/price.dat")) then
      fn = fopen("db/inventory/price.dat","rb")     inventory[iPRICE]     = get4u({fn,len})       fclose(fn)
    end if
    inventory[iCOMMENT] = repeat({},len)
    if length(dir("db/inventory/comment.dat")) then
      fn = fopen("db/inventory/comment.dat","rb")     inventory[iCOMMENT]   = get_string({fn,len})  fclose(fn)
    end if
    inventory[iSTAMP] = repeat(NULL,len)
    if length(dir("db/inventory/stamp.dat")) then
      fn = fopen("db/inventory/stamp.dat","rb")     inventory[iSTAMP]     = get4u({fn,len})       fclose(fn)
    end if
    allow_error_trapping(ON)
  end if
  set_timer(hWndMAIN,TIMER_UPDATE_INVENTORY,TIMEOUT_UPDATE_INVENTORY,timeout_update_inventory_rID)
  outputLF("OK\n",{})
end procedure

procedure update_inventory()
 integer fn
  if not update then return end if
  update = FALSE
  allow_error_trapping(OFF)
  fn = fopen("db/inventory/uid.dat","wb")       put4(fn,inventory[iUID])            fclose(fn)
  fn = fopen("db/inventory/lref.dat","wb")      put4(fn,inventory[iLREF])           fclose(fn)
  fn = fopen("db/inventory/href.dat","wb")      put4(fn,inventory[iHREF])           fclose(fn)
  fn = fopen("db/inventory/ql.dat","wb")        put4(fn,inventory[iQL])             fclose(fn)
  fn = fopen("db/inventory/name.dat","wb")      put_string(fn,inventory[iNAME])     fclose(fn)
  fn = fopen("db/inventory/owner.dat","wb")     put_string(fn,inventory[iOWNER])    fclose(fn)
  fn = fopen("db/inventory/category.dat","wb")  put_string(fn,inventory[iCATEGORY]) fclose(fn)
  fn = fopen("db/inventory/guild.dat","wb")     puts(fn,inventory[iGUILD])          fclose(fn)
  fn = fopen("db/inventory/price.dat","wb")     put4(fn,inventory[iPRICE])          fclose(fn)
  fn = fopen("db/inventory/comment.dat","wb")   put_string(fn,inventory[iCOMMENT])  fclose(fn)
  fn = fopen("db/inventory/stamp.dat","wb")     put4(fn,inventory[iSTAMP])          fclose(fn)
  allow_error_trapping(ON)
  set_timer(hWndMAIN,TIMER_UPDATE_INVENTORY,TIMEOUT_UPDATE_INVENTORY,timeout_update_inventory_rID)
end procedure

function timeout_update_inventory(atom hwnd, atom uMsg, atom idEvent, atom dwTime)
  if (hwnd = hWndMAIN) and (idEvent = TIMER_UPDATE_INVENTORY) then
    update_inventory()
  end if
  return NULL
end function
timeout_update_inventory_rID = define_c_callback(routine_id("timeout_update_inventory"))

function inv_update(sequence msg,integer admin)
  update_inventory()
  send_msg("The inventory database has been updated.",{})
  return TRUE
end function

-- add <posted item>
function inv_add(sequence msg, integer admin)
 sequence args,category,owner,player,xml,item,tmp,xmsg
 integer f,xid,share,sell,pos,count
  if admin and not grant(MAX_RIGHTS,cmd_groupID,cmd_buddyID) then return FALSE end if

  msg = trim_white(msg)
  
  if find('\"',msg) != 1 then
    send_msg("You must specify a category for the items to be added to.",{})
    return FALSE
  end if
  msg = get_args(msg,{ARGS})
  args = msg[1]
  msg = msg[2]
  if not (length(args) and length(msg)) then return FALSE end if

  msg = trim_white(msg)
  f = find_buddy(cmd_buddyID)
  owner = BUDDY[5][f]
  player = owner
  if admin and equal("player",lower(args[1])) then
    msg = get_args(msg,{ARGS,ARGS})
    args = msg[1]
    msg = msg[2]
    if (length(args) != 2) and length(msg) then return FALSE end if
    player = lower(args[1])
    args = args[2..2]
  end if
  category = lower(args[1])
  share = (match("share ",lower(msg)) = 1)
  if share then msg = ltrim_white(msg[length("share ")+1..length(msg)]) end if
  sell = (match("sell ",lower(msg)) = 1)
  if sell then msg = ltrim_white(msg[length("sell ")+1..length(msg)]) end if

  if not length(msg) or match("<a ",lower(msg)) != 1 then return FALSE end if
  count = 0
  xmsg = trim_white(msg)
  while length(xmsg) and (xmsg[1] = '<') do
    xid = xml_new(xmsg)
    xml = xml_get_tag(xid)
    if not equal(xml[1],"a") then
      xml_kill(xid)
      exit
    end if

    count += 1
    xml = xml_close_element(xid,xml,NO)

    item = repeat(NULL,length(inventory))
    tmp = xml[2][2][1] -- href="itemref://123456"
    tmp = tmp[length("itemref://")+1..length(tmp)]
    f = find('/',tmp)
    item[iUID] = count
    item[iLREF] = get_value(tmp[1..f-1])
    tmp = tmp[f+1..length(tmp)]
    f = find('/',tmp)
    item[iHREF] = get_value(tmp[1..f-1])
    item[iQL] = get_value(tmp[f+1..length(tmp)])
    item[iNAME] = lower(xml[3][1][3])
    item[iOWNER] = player
    item[iCATEGORY] = category
    item[iGUILD] = share
    item[iPRICE] = sell
    item[iCOMMENT] = ""

    pos = xml_get_pos(xid)

    xmsg = trim_white(xmsg[pos..length(xmsg)])

    if length(xmsg) then
      f = find('<',xmsg)
      if not f then f = length(xmsg)+1 end if
      item[iCOMMENT] = trim_white(strip_quotes(trim_white(xmsg[1..f-1])))
      xmsg = trim_white(xmsg[f..length(xmsg)])
    end if

    item[iSTAMP] = timestamp()
    inventory = vappend(inventory,item)

    xml_kill(xid)
  end while
  update = TRUE
  send_msg(sprintf("%d items added to \"%s\"%s",{count,capitalize(category),iff(admin and not equal(owner,player)," as "&capitalize(player),"")}),{})
  return TRUE
end function

function inv_remove(sequence msg, integer admin)
 sequence args,owner
 integer f,uid,count
 atom stamp
  if admin and not grant(MAX_RIGHTS,cmd_groupID,cmd_buddyID) then return FALSE end if

  args = tokenize(msg,' ')
  if not length(args) then return FALSE end if

  f = find_buddy(cmd_buddyID)
  owner = BUDDY[5][f]
  count = 0
  for i = 1 to length(args) do
    if length(args[i]) = 10 then -- valid uid
      stamp = get_value("#"&args[i][1..8])
      uid = get_value("#"&args[i][9..10])
      for j = length(inventory[iUID]) to 1 by -1 do
        if (inventory[iSTAMP][j] = stamp) and (inventory[iUID][j] = uid)
        and (admin or equal(owner,inventory[iOWNER][j])) then
          inventory = vsplice(inventory,j,{})
          count += 1
          update = TRUE
          exit
        end if
      end for
    end if
  end for
  send_msg(sprintf("%d items have been removed.",{count}),{})
  return TRUE
end function

function inv_share(sequence msg,integer admin)
 sequence args,owner
 integer f,uid,count,flag
 atom stamp

  if admin and not grant(MAX_RIGHTS,cmd_groupID,cmd_buddyID) then return FALSE end if

  args = tokenize(msg,' ')
  if not length(args) then return FALSE end if
  flag = find(upper(args[1]),{"YES","NO"})
  if flag then args = args[2..length(args)] end if
  if not length(args) then return FALSE end if
  flag = (flag != 2)

  f = find_buddy(cmd_buddyID)
  owner = BUDDY[5][f]
  count = 0
  for i = 1 to length(args) do
    if length(args[i]) = 10 then -- valid uid
      stamp = get_value("#"&args[i][1..8])
      uid = get_value("#"&args[i][9..10])
      for j = length(inventory[iUID]) to 1 by -1 do
        if (inventory[iSTAMP][j] = stamp) and (inventory[iUID][j] = uid)
        and (admin or equal(owner,inventory[iOWNER][j])) then
          inventory[iGUILD][j] = flag
          count += 1
          update = TRUE
          exit
        end if
      end for
    end if
  end for

  send_msg(sprintf("%d items have been "&iff(flag,"added for","removed from")&" sharing.",{count}),{})
  return TRUE
end function

function inv_sell(sequence msg,integer admin)
 sequence args,owner
 integer f,uid,count,flag
 atom stamp

  if admin and not grant(MAX_RIGHTS,cmd_groupID,cmd_buddyID) then return FALSE end if

  args = tokenize(msg,' ')
  if not length(args) then return FALSE end if
  flag = find(upper(args[1]),{"YES","NO"})
  if flag then args = args[2..length(args)] end if
  if not length(args) then return FALSE end if
  flag = (flag != 2)

  f = find_buddy(cmd_buddyID)
  owner = BUDDY[5][f]
  count = 0
  for i = 1 to length(args) do
    if length(args[i]) = 10 then -- valid uid
      stamp = get_value("#"&args[i][1..8])
      uid = get_value("#"&args[i][9..10])
      for j = length(inventory[iUID]) to 1 by -1 do
        if (inventory[iSTAMP][j] = stamp) and (inventory[iUID][j] = uid)
        and (admin or equal(owner,inventory[iOWNER][j])) then
          inventory[iPRICE][j] = flag
          count += 1
          update = TRUE
          exit
        end if
      end for
    end if
  end for

  send_msg(sprintf("%d items have been "&iff(flag,"set for","removed from")&" sale.",{count}),{})
  return TRUE
end function

-- inv
-- inv update
-- inv list
-- inv search
-- inv add
-- inv remove
-- inv share
-- inv sell

-- FLAG: yes || no
-- ADMIN: admin [<FLAG>]
-- PLAYER: player <TOKEN>
-- CATEGORY: category <TOKEN>
-- SHARE: share [<FLAG>]
-- OP: and || or
-- SELL: sell [<FLAG>]
-- QL: ql [from] <NUM> [[to] <NUM>]
-- PAGE: page <NUM>
-- QUERY: <STRING>*
-- ITEMREF: <a href="itemref://?*/?*/?*">?*</a>
-- UID: <HEX>*10

-- inv
-- inv update
-- inv list [<ADMIN>] [<PLAYER>] [<CATEGORY>] [<SHARE> [[<OP>] <SELL>]] [<QL>] [<PAGE>] [<QUERY>]
-- inv search [<ADMIN>] [<PLAYER>] [<CATEGORY>] [<SHARE> [[<OP>] <SELL>]] [<QL>] [<PAGE>] [<QUERY>]
-- inv add [<ADMIN> <PLAYER>] <CATEGORY> (<ITEMREF> [<STRING>])*
-- inv remove [<ADMIN>] <UID>*
-- inv share [<ADMIN>] <UID>*
-- inv sell [<ADMIN>] <UID>*

constant
      ilOWNER = enum_start(1,1)
     ,ilPLAYER = enum()
     ,ilCATEGORY = enum()
     ,ilSHARED = enum()
     ,ilOP = enum()
     ,ilSELL = enum()
     ,ilQLLO = enum()
     ,ilQLHI = enum()
     ,ilQUERY = enum()
     ,ilPAGE = enum()

function get_inv_list_args(sequence msg)
 sequence owner,player,category
 integer f,shared,sell,op,qllo,qlhi,page
 object tmp

  f = find_buddy(cmd_buddyID)
  owner = BUDDY[5][f]
  player = ""
  if not length(trim_white(msg)) then player = owner end if
  category = ""
  shared = EOF
  sell = EOF
  op = NULL -- 0 = AND, 1 = OR
  qllo = 1
  qlhi = 999
  page = 0
  tmp = get_token(msg," ")
  if equal(tmp[1],"player") then
    msg = tmp[2]    tmp = get_token(msg," ")
    player = trim_white(strip_quotes(tmp[1]))
    msg = tmp[2]    tmp = get_token(msg," ")
  end if
  if equal(tmp[1],"category") then
    msg = tmp[2]    tmp = get_token(msg," ")
    category = trim_white(strip_quotes(tmp[1]))
    msg = tmp[2]    tmp = get_token(msg," ")
  end if
  if equal(tmp[1],"shared") then
    msg = tmp[2]    tmp = get_token(msg," ")
    shared = find(trim_white(strip_quotes(tmp[1])),{"yes","no"})
    if shared then
      msg = tmp[2]    tmp = get_token(msg," ")
    end if
    shared = (shared < 2)
  end if
  if (shared != EOF) then
    op = find(tmp[1],{"and","or"})
    if op then
      op -= 1
      msg = tmp[2]    tmp = get_token(msg," ")
    end if
  end if
  if equal(tmp[1],"sell") then
    msg = tmp[2]    tmp = get_token(msg," ")
    sell = find(trim_white(strip_quotes(tmp[1])),{"yes","no"})
    if sell then
      msg = tmp[2]    tmp = get_token(msg," ")
    end if
    sell = (sell < 2)
  end if
  if equal(tmp[1],"ql") then
    qllo = 0
    qlhi = 0
    msg = tmp[2]    tmp = get_token(msg," ")
    f = equal(tmp[1],"from")
    if f then msg = tmp[2]    tmp = get_token(msg," ") end if
    if length(tmp[1]) and not find(FALSE,digit_char(tmp[1])) then
      qllo = get_value(tmp[1])
      msg = tmp[2]    tmp = get_token(msg," ")
    end if
    if equal(tmp[1],"to") then f += 1     msg = tmp[2]    tmp = get_token(msg," ") end if
    if length(tmp[1]) and not find(FALSE,digit_char(tmp[1])) then
      qlhi = get_value(tmp[1])
      msg = tmp[2]    tmp = get_token(msg," ")
    elsif qllo and not f then
      qlhi = qllo
    end if
    if (qllo <= 0) then qllo = 1 end if
    if (qlhi <= 0) then qlhi = 999 end if
  end if
  if equal(tmp[1],"page") then
    tmp = get_token(tmp[2]," ")
    page = get_value(tmp[1])
    msg = tmp[2]    tmp = get_token(msg," ")
  end if
  return {owner,player,category,shared,op,sell,qllo,qlhi,trim_white(strip_quotes(trim_white(msg))),page}
end function

function filter_inv_list(sequence args)
 sequence results

  results = {}
  args[ilSHARED] = ((args[ilSHARED] = EOF) or ((inventory[iGUILD] != FALSE) = args[ilSHARED]))
  args[ilSELL] = ((args[ilSELL] = EOF) or ((inventory[iPRICE] != FALSE) = args[ilSELL]))
  if args[ilOP] then  args[ilOP] = (args[ilSHARED] or args[ilSELL])
  else                args[ilOP] = (args[ilSHARED] and args[ilSELL])
  end if
  for i = 1 to length(inventory[iOWNER]) do
    if args[ilOP][i]
    and (not length(args[ilPLAYER])   or wildcard_match(args[ilPLAYER],inventory[iOWNER][i]))
    and (not length(args[ilCATEGORY]) or wildcard_match(args[ilCATEGORY],inventory[iCATEGORY][i]))
    and (inventory[iQL][i] >= args[ilQLLO]) and (inventory[iQL][i] <= args[ilQLHI])
    then results &= i end if
  end for
  return results
end function

function categorize_inv_list(sequence results)
 sequence tmp
 integer f
  -- sort into categories
  tmp = results
  results = {{},{}}
  for i = 1 to length(tmp) do
    f = find(inventory[iCATEGORY][tmp[i]],results[1])
    if not f then
      results[1] &={ inventory[iCATEGORY][tmp[i]] }
      results[2] &={ {} }
      f = length(results[1])
    end if
    results[2][f] &= tmp[i]
  end for
  return results
end function

function query_inv_list(sequence results, sequence query)
 sequence ws,s,tmp,scores
 integer min, max, maxscore,score,count

  ws = get_whitespace()
  set_whitespace({NULL})

  -- split into tokens
  s = query * alphanum_char(query)
  s = lower(trim_white(compress_white(s)))
  set_whitespace(ws)
  if not length(s) then return {{},{}} end if

  s = tokenize(s,NULL)
  maxscore = length(s)+1
  min = maxscore
  max = 1
  tmp = results
  results = repeat({},maxscore)
  scores = repeat(0,maxscore)
  count = 0
  for i = 1 to length(tmp) do
    if equal(query,inventory[iNAME][tmp[i]]) then -- exact match
      results[maxscore] &= tmp[i]
      scores[maxscore] += 1
      count += 1
      if min > maxscore then min = maxscore end if
      if max < maxscore then max = maxscore end if
    end if
  end for
  if not count then
    for i = 1 to length(tmp) do
      if length(inventory[iNAME][tmp[i]]) then
        score = 0
        for j = 1 to length(s) do
          if score+length(s)-j+1 < max then score = 0    exit end if -- can't score high enough to matter
          if match(s[j],inventory[iNAME][tmp[i]]) then score += 1 end if
        end for
        if score then
          scores[score] += 1
          count += 1
          if min > score then min = score end if
          if max < score then max = score end if
          if count < MAX_RESULTS then
            results[score] &= tmp[i]
          elsif score = max then
            results[score] &= tmp[i]
          end if
        end if
      end if
    end for
  end if

  return {count,maxscore,min,max,scores,results}
end function

procedure inv_list_query_results(sequence results, sequence args, integer admin)
 sequence item,cmd
 string out
 integer flag
 atom p1,p2
 object tmp

  tmp = results
  results = tmp[6][tmp[4]]
  out = "<font color=CCInfoText>"
  out &= sprintf("%d results scored %d out of %d\n",{length(results),tmp[4],tmp[2]})

  tmp = {}
  for i = 1 to length(results) do -- sort by name and ql
    flag = FALSE
    if length(tmp) then
      for j = length(tmp) to 1 by -1 do
        flag = compare(inventory[iNAME][results[i]],inventory[iNAME][tmp[j]])
        if not flag then flag = compare(inventory[iQL][results[i]],inventory[iQL][tmp[j]]) end if
        if flag >= 0 then
          tmp = tmp[1..j] & results[i] & tmp[j+1..length(tmp)]
          flag = TRUE
          exit
        end if
        flag = FALSE
      end for
    end if
    if not flag then tmp = results[i] & tmp end if
  end for
  results = tmp

  if not args[ilPAGE] then
    for i = 1 to length(results) do
      item = vindex(inventory,results[i])
      tmp = sprintf("%d. ",{i})
      tmp &= " <a href='itemref://%2/%3/%4'>%5</a> (QL %4)%6<br>"
      out &= parse_command(tmp,{
                 sprintf("%08x%02x",{item[iSTAMP],item[iUID]})
                ,sprintf("%d",{item[iLREF]})
                ,sprintf("%d",{item[iHREF]})
                ,sprintf("%d",{item[iQL]})
                ,item[iNAME]
                ,iff(length(item[iCOMMENT])," <font color=white>&quot;"&item[iCOMMENT]&"&quot;</font>","")
             })
      if i = 3 then exit end if
    end for
  end if
  tmp = "more"
  if length(results) > 25 then
    if args[ilPAGE] then
      tmp = sprintf("extended results, showing page %d",{args[ilPAGE]})
    else
      args[ilPAGE] = 1
    end if
  end if
  out &= tmp&".. <a href=\"text://<font color=CCInfoText>"
  out &= "<center><font color=CCInfoHeadline><u>Extended Search Results</u></font></center><br>"

  if args[ilPAGE] then
    args[ilQUERY] = txt2xml("\""&args[ilQUERY]&"\"")
    p2 = floor(length(results)/25) + 1
    if args[ilPAGE] > p2 then args[ilPAGE] = p2 end if
    p1 = floor((args[ilPAGE]-1)/10) + 1
    out &= "<center><font color=CCInfoHeader>"
    out &= sprintf("Page %d of %d<br>",{args[ilPAGE],p2})
    cmd = parse_command("chatcmd:///tell %m %!inv list%1%2%3%4%5%6 ql from %7 to %8 page ",{
                  iff(admin != EOF," admin "&iff(admin,"YES","NO"),"")
                 ,iff(length(args[ilPLAYER])," player &quot;"&args[ilPLAYER]&"&quot;","")
                 ,iff(length(args[ilCATEGORY]),txt2xml(" category \""&args[ilCATEGORY]&"\""),"")
                 ,iff(args[ilSHARED] != EOF," shared "&iff(args[ilSHARED],"YES","NO"),"")
                 ,iff((args[ilSHARED] != EOF) and (args[ilSELL] != EOF),iff(args[ilOP]," OR"," AND"),"")
                 ,iff(args[ilSELL] != EOF," sell "&iff(args[ilSELL],"YES","NO"),"")
                 ,sprintf("%d",{args[ilQLLO]})
                 ,sprintf("%d",{args[ilQLHI]})
               })
    p2 = floor(length(results)/25)+1
    if p1+9 < p2 then p2 = p1+9 end if
    if p1 > 1 then out &= "<a href='"&cmd&sprintf("%d ",{p1-9})&args[ilQUERY]&"'>&lt;&lt;</a> " end if
    for i = p1 to p2 do
      if i = args[ilPAGE] then
        out &= sprintf("%d ",{i})
      else
        out &= "<a href='"&cmd&sprintf("%d "&args[ilQUERY]&"'>%d</a> ",{i,i})
      end if
    end for
    if p2*25 < length(results) then out &= "<a href='chatcmd:///"&cmd&sprintf("%d ",{p2+1})&args[ilQUERY]&"'>&gt;&gt;</a>" end if
    out &= "</font></center><br>"
  end if

  p1 = 1
  p2 = length(results)
  if p2 > 25 then
    if not args[ilPAGE] or ((args[ilPAGE]*25 - 25) > p2) then args[ilPAGE] = 1 end if
    p1 = (args[ilPAGE]-1)*25
    if p1+25 < p2 then p2 = p1+25 end if
    p1 += 1
  else
    args[ilPAGE] = 0
  end if
  for i = p1 to p2 do
    item = vindex(inventory,results[i])
    tmp = sprintf("%d. ",{i})
    tmp &= "[<a href='chatcmd:///tell %m %!inv list player "&txt2xml("\"%6\"")&" category "&txt2xml("\"%7\"")&"%8%9%10'>?</a>]"
    tmp &= "[<a href='chatcmd:///tell %6'>@</a>]"
    tmp &= iff(item[iGUILD],"[*]","")
    tmp &= iff(item[iPRICE],"[$]","")
    tmp &= " <a href='itemref://%2/%3/%4'>%5</a> (QL %4)%11<br>"
    out &= parse_command(tmp,{
               sprintf("%08x%02x",{item[iSTAMP],item[iUID]})
              ,sprintf("%d",{item[iLREF]})
              ,sprintf("%d",{item[iHREF]})
              ,sprintf("%d",{item[iQL]})
              ,item[iNAME]
              ,item[iOWNER]
              ,item[iCATEGORY]
              ,iff(args[ilSHARED] != EOF," shared "&iff(args[ilSHARED],"YES","NO"),"")
              ,iff((args[ilSHARED] != EOF) and (args[ilSELL] != EOF)," "&iff(args[ilOP],"OR","AND"),"")
              ,iff(args[ilSELL] != EOF," sell "&iff(args[ilSELL],"YES","NO"),"")
              ,iff(length(item[iCOMMENT])," <font color=white>&quot;"&item[iCOMMENT]&"&quot;</font>","")
           })
  end for
  out &= "</font>\">here</a></font>"
  send_msg(out,{})
end procedure

procedure inv_list_item_results(sequence results, sequence args, integer admin)
 sequence item,uids,cmd
 string out
 integer flag,shared,sell,p1,p2
 object tmp

  results = results[2][1]
  tmp = repeat(repeat(NULL,length(results)),length(inventory))
  for i = 1 to length(results) do
    for j = 1 to length(tmp) do
      tmp[j][i] = inventory[j][results[i]]
    end for
  end for
  results = vsort(vreverse(vsort(tmp,iNAME)),iQL)

  p1 = 1
  p2 = length(results[iNAME])
  if p2 > 25 then
    if not args[ilPAGE] or ((args[ilPAGE]*25 - 25) > p2) then args[ilPAGE] = 1 end if
    p1 = (args[ilPAGE]-1)*25
    if p1+25 < p2 then p2 = p1+25 end if
    p1 += 1
  else
    args[ilPAGE] = 0
  end if

  uids = repeat(NULL,p2-p1+1)
  shared = FALSE
  sell = FALSE
  tmp = {}

  for i = p1 to p2 do
    shared += (results[iGUILD][i] != FALSE)
    sell += (results[iPRICE][i] != FALSE)
    uids[i-p1+1] = sprintf("%08x%02x",{results[iSTAMP][i],results[iUID][i]})
  end for
  shared = (shared = length(results[iNAME])) - (not shared) -- none shared = FALSE, mixed shared = 0, all shared = TRUE
  sell = (sell = length(results[iNAME])) - (not sell)

  flag = admin and not equal(args[ilOWNER],args[ilPLAYER])

  out = ""
  if admin then
    out &= parse_command("<center><font color=CCInfoHeader>"
        & "[<a href='chatcmd:///tell %m %!inv"&iff(flag," admin","")&" remove%1'>X</a>]"
        & "[<a href='chatcmd:///tell %m %!inv"&iff(flag," admin","")&" share "
            &iff(shared=TRUE,"NO","YES")&"%1'>*</a>"&iff(shared,"<font color="&iff(shared=TRUE,"white>Y","red>N")&"</font>","")&"]"
        & "[<a href='chatcmd:///tell %m %!inv"&iff(flag," admin","")&" sell "
            &iff(sell=TRUE,"NO","YES")&"%1'>$</a>"&iff(sell,"<font color="&iff(sell=TRUE,"white>Y","red>N")&"</font>","")&"]"
        & "(ALL)</font><br></center>"
        & "<br>"
         ,{flatten( vprepend( uids, repeat(' ',length(uids)) ) )})
  end if
  
  for i = p1 to p2 do
    if admin then
      tmp =  "[<a href='chatcmd:///tell %m %!inv"&iff(flag," admin","")&" remove %1'>X</a>]"
      tmp &= "[<a href='chatcmd:///tell %m %!inv"&iff(flag," admin","")&" share "&iff(results[iGUILD][i],"NO","YES")&" %1'>*</a><font color="&iff(results[iGUILD][i],"white>Y","red>N")&"</font>]"
      tmp &= "[<a href='chatcmd:///tell %m %!inv"&iff(flag," admin","")&" sell "&iff(results[iPRICE][i],"NO","YES")&" %1'>$</a><font color="&iff(results[iPRICE][i],"white>Y","red>N")&"</font>]"
    else
      tmp =  "[<a href='chatcmd:///tell %m %!inv list player "&txt2xml("\"%6\"")&" category "&txt2xml("\"%7\"")&"%8%9%10 ql from %11 to %12'>?</a>]"
      tmp &= "[<a href='chatcmd:///tell %6'>@</a>]"
      tmp &= iff(results[iGUILD][i],"[*]","")
      tmp &= iff(results[iPRICE][i],"[$]","")
    end if
    tmp &= " <a href='itemref://%2/%3/%4'>%5</a> (QL %4)%13<br>"
    out &= parse_command(tmp,{
               uids[i-p1+1]
              ,sprintf("%d",{results[iLREF][i]})
              ,sprintf("%d",{results[iHREF][i]})
              ,sprintf("%d",{results[iQL][i]})
              ,results[iNAME][i]
              ,results[iOWNER][i]
              ,results[iCATEGORY][i]
              ,iff(args[ilSHARED] != EOF," shared "&iff(args[ilSHARED],"YES","NO"),"")
              ,iff((args[ilSHARED] != EOF) and (args[ilSELL] != EOF)," "&iff(args[ilOP],"OR","AND"),"")
              ,iff(args[ilSELL] != EOF," sell "&iff(args[ilSELL],"YES","NO"),"")
              ,sprintf("%d",{args[ilQLLO]})
              ,sprintf("%d",{args[ilQLHI]})
              ,iff(length(results[iCOMMENT][i])," <font color=white>&quot;"&results[iCOMMENT][i]&"&quot;</font>","")
           })
  end for
  if length(args[ilCATEGORY]) then
    tmp = "Category: '"&capitalize(args[ilCATEGORY])&"'<br>"
  else
    tmp = ""
  end if
  if args[ilPAGE] then
    p2 = floor(length(results[iNAME])/25) + 1
    if args[ilPAGE] > p2 then args[ilPAGE] = p2 end if
    p1 = floor((args[ilPAGE]-1)/10) + 1
    tmp &= "<br>"
    tmp &= sprintf("Page %d of %d<br>",{args[ilPAGE],p2})
    cmd = parse_command("chatcmd:///tell %m %!inv%1 list%2%3%4%5%6 ql from %7 to %8 page ",{
                  iff(admin != EOF," admin "&iff(admin,"YES","NO"),"")
                 ,iff(length(args[ilPLAYER])," player "&args[ilPLAYER],"")
                 ,iff(length(args[ilCATEGORY]),txt2xml(" category \""&args[ilCATEGORY]&"\""),"")
                 ,iff(args[ilSHARED] != EOF," shared "&iff(args[ilSHARED],"YES","NO"),"")
                 ,iff((args[ilSHARED] != EOF) and (args[ilSELL] != EOF),iff(args[ilOP]," OR"," AND"),"")
                 ,iff(args[ilSELL] != EOF," sell "&iff(args[ilSELL],"YES","NO"),"")
                 ,sprintf("%d",{args[ilQLLO]})
                 ,sprintf("%d",{args[ilQLHI]})
               })
    p2 = floor(length(results[iNAME])/25)+1
    if p1+9 < p2 then p2 = p1+9 end if
    if p1 > 1 then tmp &= "<a href='"&cmd&sprintf("%d",{p1-9})&"'>&lt;&lt;</a> " end if
    for i = p1 to p2 do
      if i = args[ilPAGE] then
        tmp &= sprintf("%d ",{i})
      else
        tmp &= "<a href='"&cmd&sprintf("%d'>%d</a> ",{i,i})
      end if
    end for
    if p2*25 < length(results[iNAME]) then tmp &= "<a href='chatcmd:///"&cmd&sprintf("%d",{p2+1})&"'>&gt;&gt;</a>" end if
    tmp &= "<br>"
  end if

  if args[ilSHARED] != EOF then tmp &= "SHARED "&iff(args[ilSHARED],"YES","NO") end if
  if (args[ilSHARED] != EOF) and (args[ilSELL] != EOF) then tmp &= iff(args[ilOP]," OR "," AND ") end if
  if args[ilSELL] != EOF then tmp &= "SELL "&iff(args[ilSELL],"YES","NO") end if
  if (args[ilSHARED] != EOF) or (args[ilSELL] != EOF) then tmp &= "<br>" end if
  args[ilPLAYER] *= not ((args[ilPLAYER] = '*') or (args[ilPLAYER] = '?'))
  args[ilPLAYER] += (args[ilPLAYER] = 0) * ' '
  args[ilPLAYER] = trim_white(args[ilPLAYER])
  if not length(args[ilPLAYER]) then args[ilPLAYER] = ORGNAME end if
  run_script(cmd_hsock,cmd_msgID,cmd_groupID,cmd_buddyID,"inventory.txt \""&capitalize(args[ilPLAYER])&"\" \""&tmp&"\" \"<br>"&out&"\"")
end procedure

procedure inv_list_category_results(sequence results,sequence args,integer admin)
 string out,cmd
 integer flag,p1,p2,share,sell,count,qllo,qlhi
 object tmp

  flag = admin and not equal(args[ilOWNER],args[ilPLAYER])
  results = vsort(results,1)

  p1 = 1
  p2 = length(results[1])
  if p2 > 25 then
    if not args[ilPAGE] or ((args[ilPAGE]*25 - 25) > p2) then args[ilPAGE] = 1 end if
    p1 = (args[ilPAGE]-1)*25
    if p1+25 < p2 then p2 = p1+25 end if
    p1 += 1
  else
    args[ilPAGE] = 0
  end if

  out = ""
  for i = p1 to p2 do
    share = FALSE
    sell = FALSE
    qllo = inventory[iQL][results[2][i][1]]
    qlhi = inventory[iQL][results[2][i][1]]
    count = length(results[2][i])
    for j = 1 to count do
      share += inventory[iGUILD][results[2][i][j]]
      sell  += inventory[iPRICE][results[2][i][j]]
      tmp = inventory[iQL][results[2][i][j]]
      if tmp < qllo then qllo = tmp
      elsif tmp > qlhi then qlhi = tmp
      end if
    end for
    out &= "[<font color="&iff(share,iff(share=count,"#00CC00","#FFFF00"),"#FF0000")&">*</font>]"
    out &= "[<font color="&iff(sell,iff(sell=count,"#00CC00","YELLOW"),"RED")&">$</font>]"
    out &= parse_command(" <a href='chatcmd:///tell %m %!inv%1 list%2%3%4%5%6'>"&capitalize(results[1][i])&"</a> (%7 items QL %8 to %9) <br>"
              ,{iff(flag," admin","")
                ,iff(length(args[ilPLAYER])," player "&args[ilPLAYER],"")
                ,txt2xml(" category \""&results[1][i]&"\"")
                ,iff(args[ilSHARED] != EOF," shared "&iff(args[ilSHARED],"YES","NO"),"")
                ,iff((args[ilSHARED] != EOF) and (args[ilSELL] != EOF)," "&iff(args[ilOP],"OR","AND"),"")
                ,iff(args[ilSELL] != EOF," sell "&iff(args[ilSELL],"YES","NO"),"")
                ,count
                ,qllo
                ,qlhi
               })
  end for
  tmp = "Category Listing<br>"

  if args[ilPAGE] then
    p2 = floor(length(results[1])/25) + 1
    if args[ilPAGE] > p2 then args[ilPAGE] = p2 end if
    p1 = floor((args[ilPAGE]-1)/10) + 1
    tmp &= "<br>"
    tmp &= sprintf("Page %d of %d<br>",{args[ilPAGE],p2})
    cmd = parse_command("chatcmd:///tell %m %!inv%1 list%2%3%4%5%6 ql from %7 to %8 page ",{
                  iff(admin != EOF," admin "&iff(admin,"YES","NO"),"")
                 ,iff(length(args[ilPLAYER])," player "&args[ilPLAYER],"")
                 ,iff(length(args[ilCATEGORY]),txt2xml(" category \""&args[ilCATEGORY]&"\""),"")
                 ,iff(args[ilSHARED] != EOF," shared "&iff(args[ilSHARED],"YES","NO"),"")
                 ,iff((args[ilSHARED] != EOF) and (args[ilSELL] != EOF),iff(args[ilOP]," OR"," AND"),"")
                 ,iff(args[ilSELL] != EOF," sell "&iff(args[ilSELL],"YES","NO"),"")
                 ,sprintf("%d",{args[ilQLLO]})
                 ,sprintf("%d",{args[ilQLHI]})
               })
    p2 = floor(length(results[1])/25)+1
    if p1+9 < p2 then p2 = p1+9 end if
    if p1 > 1 then tmp &= "<a href='"&cmd&sprintf("%d",{p1-9})&"'>&lt;&lt;</a> " end if
    for i = p1 to p2 do
      if i = args[ilPAGE] then
        tmp &= sprintf("%d ",{i})
      else
        tmp &= "<a href='"&cmd&sprintf("%d'>%d</a> ",{i,i})
      end if
    end for
    if p2*25 < length(results[1]) then tmp &= "<a href='chatcmd:///"&cmd&sprintf("%d",{p2+1})&"'>&gt;&gt;</a>" end if
    tmp &= "<br>"
  end if

  if args[ilSHARED] != EOF then tmp &= "SHARED "&iff(args[ilSHARED],"YES","NO") end if
  if (args[ilSHARED] != EOF) and (args[ilSELL] != EOF) then tmp &= iff(args[ilOP]," OR "," AND ") end if
  if args[ilSELL] != EOF then tmp &= "SELL "&iff(args[ilSELL],"YES","NO") end if
  if (args[ilSHARED] != EOF) or (args[ilSELL] != EOF) then tmp &= "<br>" end if
  args[ilPLAYER] *= not ((args[ilPLAYER] = '*') or (args[ilPLAYER] = '?'))
  args[ilPLAYER] += (args[ilPLAYER] = 0) * ' '
  args[ilPLAYER] = trim_white(args[ilPLAYER])
  if not length(args[ilPLAYER]) then args[ilPLAYER] = ORGNAME end if
  run_script(cmd_hsock,cmd_msgID,cmd_groupID,cmd_buddyID,"inventory.txt \""&capitalize(args[ilPLAYER])&"\" \""&tmp&"\" \"<br>"&out&"\"")
end procedure

function inv_list_results(sequence args, integer admin)
 sequence results
 integer flag
 
  if (admin = EOF) then
    admin = equal(args[ilOWNER],args[ilPLAYER])
  elsif admin and not equal(args[ilOWNER],args[ilPLAYER]) then
    admin = grant(MAX_RIGHTS,cmd_groupID,cmd_buddyID)
  end if
  flag = TRUE
  if not admin and not equal(args[ilOWNER],args[ilPLAYER]) then
    -- public access
    if (args[ilSHARED] = EOF) and (args[ilSELL] = EOF) then -- no filters specified, default to "shared OR sell"
      args[ilSHARED] = TRUE
      args[ilOP] = 1 -- OR
      args[ilSELL] = TRUE
    end if
  end if

  results = filter_inv_list(args)
  
  if length(args[ilQUERY]) then
    results = query_inv_list(results,args[ilQUERY])
    if results[1] then
      inv_list_query_results(results,args,admin)
      return TRUE
    end if
  elsif length(results) then
    results = categorize_inv_list(results)
    if length(results[1]) = 1 then -- show items in specified category
      inv_list_item_results(results,args,admin)
    else
      inv_list_category_results(results,args,admin)
    end if
    return TRUE
  end if
  send_msg("There are no items in the inventory database that match your query.",{})
  return TRUE
end function

-- show player's inventory by category
function inv_list(sequence msg, integer admin)
 object tmp

  msg = trim_white(msg)
  tmp = get_inv_list_args(msg)
  
  return inv_list_results(tmp,admin)
end function

function get_admin_arg(sequence msg)
 sequence tmp
 integer admin
  tmp = get_token(msg," ")
  admin = EOF
  if equal(tmp[1],"admin") then
    msg = tmp[2]    tmp = get_token(msg," ")
    admin = find(tmp[1],{"yes","no"})
    if admin then msg = tmp[2] end if
    admin = (admin < 2)
  end if
  return {admin,msg}
end function

constant INV_ARGS = {"list","add","remove","share","sell","update"}
constant INV_RIDS = {
      routine_id("inv_list")
     ,routine_id("inv_add")
     ,routine_id("inv_remove")
     ,routine_id("inv_share")
     ,routine_id("inv_sell")
     ,routine_id("inv_update")
}

procedure cmd_inv(sequence args, sequence msg)
 sequence tmp
 integer admin,mode
-- trace(ON)
  msg = lower(msg)

  tmp = get_admin_arg(msg)
  admin = tmp[1]
  msg = tmp[2]

  tmp = get_token(msg," ")
  mode = find(tmp[1],INV_ARGS)
  if mode then msg = tmp[2] else mode = 1 end if

  if grant("inv "&INV_ARGS[mode],cmd_groupID,cmd_buddyID) then
    if call_func(INV_RIDS[mode],{msg,admin}) then return end if
  end if
  run_help(cmd_hsock,cmd_msgID,cmd_groupID,cmd_buddyID,{"inv"})
end procedure
new_command("inv",routine_id("cmd_inv"),{})

load_inventory()
