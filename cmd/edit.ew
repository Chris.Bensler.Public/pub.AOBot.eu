-- edit [mode] [[line] [from] start [[to] stop]] "path/to/filename.txt"
-- read
-- write
-- insert
-- delete
-- browse

-- edit read [[from] line1 [[to] line2]] "path/to/filename.txt"
-- edit write [[from] line1 [[to] line2]] "path/to/filename.txt"
-- edit delete [[from] line1 [[to] line2]] "path/to/filename.txt"
-- edit new "path/to/filename.txt"
-- edit browse "path/to/filename.txt"

with trace

--! function edit_get_args(sequence base, sequence msg)
--!  sequence s,tmp,path
--!  integer mode,start,stop
--!  integer lflag,fflag,tflag
--!  integer pos,fn
--! 
--!   s = lower(msg)
--!   pos = length(msg)-length(s)+1
--!   tmp = get_token(s," ")
--!   mode = find(tmp[1],EDIT_ARGS)
--!   if mode then -- get the next token
--!     s = tmp[2]
--!     pos = length(msg)-length(s)+1
--!     tmp = get_token(s," ")
--!   else -- default to read
--!     mode = 1
--!   end if
--!   lflag = equal(tmp[1],"line")
--!   if lflag then -- get the next token
--!     s = tmp[2]
--!     pos = length(msg)-length(s)+1
--!     tmp = get_token(s," ")
--!   end if
--!   fflag = equal(tmp[1],"from")
--!   if fflag then -- get the next token
--!     s = tmp[2]
--!     pos = length(msg)-length(s)+1
--!     tmp = get_token(s," ")
--!   end if
--!   start = 0
--!   if length(tmp[1]) and not find(FALSE,digit_char(tmp[1])) then -- get the next token
--!     start = get_value(tmp[1])
--!     s = tmp[2]
--!     pos = length(msg)-length(s)+1
--!     tmp = get_token(s," ")
--!   elsif lflag or fflag then -- expected a numeric value
--!     send_msg(parse_command("Expected a numeric value.\n%!edit %1 %lt;--",{msg[1..pos-1]}),{})
--!     mode = 0
--!   end if
--!   tflag = equal(tmp[1],"to")
--!   if tflag then -- get the next token
--!     s = tmp[2]
--!     pos = length(msg)-length(s)+1
--!     tmp = get_token(s," ")
--!   end if
--!   stop = 0
--!   if length(tmp[1]) and not find(FALSE,digit_char(tmp[1])) then -- get the next token
--!     stop = get_value(tmp[1])
--!     s = tmp[2]
--!     pos = length(msg)-length(s)+1
--!     tmp = get_token(s," ")
--!   elsif tflag then -- expected a numeric value
--!     send_msg(parse_command("Expected a numeric value.\n%!edit %1 &lt;--",{msg[1..pos-1]}),{})
--!     mode = 0
--!   end if
--! 
--!   path = trim_white(strip_quotes(tmp[1]))
--!   if length(path) and (path[1] != '/') then path = "/"&path end if
--!   if mode = 5 then
--!     if not length(path) then path = "/" end if
--!     if length(dir(base&path)) < 2 then
--!       send_msg(parse_command("Invalid directory path.\n%!edit %1 <--",{msg[1..pos-1]}),{})
--!       mode = 0
--!     end if
--!   elsif length(dir(base&path)) < 1 then
--!     send_msg(parse_command("Invalid file path.\n%!edit %1 <--",{msg[1..pos-1]}),{})
--!     mode = 0
--!   end if
--!   return {mode,start,stop,path}
--! end function

function cmd_edit_read(sequence base, sequence msg)
 sequence path,lines,line,tmp
 integer lflag,fflag,tflag
 integer l1,l2,lcount
 integer page,p1,p2,pcount
 atom len

  l1 = 1
  l2 = EOF

  tmp = get_token(msg," ")
  lflag = equal(tmp[1],"line")
  if lflag then
    msg = tmp[2]
    tmp = get_token(msg," ")
  end if
  fflag = equal(tmp[1],"from")
  if fflag then
    msg = tmp[2]
    tmp = get_token(msg," ")
  end if
  if length(tmp[1]) and not find(FALSE,digit_char(tmp[1])) then
    l1 = get_value(tmp[1])
    if not fflag then l2 = l1 end if
    msg = tmp[2]
    tmp = get_token(msg," ")
  elsif lflag or fflag then
    send_msg("Expected a line number.",{})
    return FALSE
  end if
  tflag = equal(tmp[1],"to")
  if tflag then
    msg = tmp[2]
    tmp = get_token(msg," ")
  end if
  if length(tmp[1]) and not find(FALSE,digit_char(tmp[1])) then
    l2 = get_value(tmp[1])
    msg = tmp[2]
    tmp = get_token(msg," ")
  elsif tflag then
    send_msg("Expected a line number.",{})
    return FALSE
  end if

  path = trim_white(strip_quotes(tmp[1]))
  len = length(dir(base&path))
  if len = 1 then
    msg = tmp[2]
    tmp = get_token(msg," ")
  elsif len > 1 then
    send_msg("No file specified.",{})
    return FALSE
  else -- len = 0
    send_msg("Invalid path.",{})
    return FALSE
  end if

  page = equal(tmp[1],"page")
  if page then
    msg = tmp[2]
    tmp = get_token(msg," ")
    page = get_value(tmp[1])
    if not (page and length(tmp[1])) or find(FALSE,digit_char(tmp[1])) then
      send_msg("Expected a page number.",{})
      return FALSE
    end if
    msg = tmp[2]
    tmp = get_token(msg," ")
  end if

  lines = fload_txt(base&path)
  if drop_error(ERROR_FOPEN) then return FALSE end if

  lcount = length(lines)

  -- crop to last line
  if l2 = EOF then l2 = lcount end if
  if l2 <= length(lines) then lines = lines[1..l2] end if

  -- crop to first line
  if l1 <= length(lines) then lines = lines[l1..length(lines)] else lines = {} end if

  p1 = l1
  p2 = l2

  tmp = ""
  len = length(lines)/25
  if not integer(len) then len = floor(len)+1 end if
  pcount = len
  if (page > pcount) then page = pcount end if
  if not page then page = 1 end if
  if (pcount > 1) then
    -- crop to first line
    lines = lines[((page-1)*25)+1..length(lines)]
    -- crop to last line
    if (length(lines) > 25) then lines = lines[1..25] end if

    tmp &= sprintf("page %d of %d<br>",{page,pcount})
    p1 = floor((page-1)/10) * 10 + 1
    p2 = pcount
    if p1 != 1 then
      tmp &= "<a href='chatcmd:///tell %m %!"&sprintf("edit read from %d to %d &quot;%s&quot; page %d'>&lt;&lt;</a> ",{l1,l2,path,p1})
    end if
    if p2 >= (p1+10) then p2 = p1+10-1 end if
    for i = p1 to p2 do
      if i = page then
        tmp &= sprintf("%d ",{i})
      else
        tmp &= "<a href='chatcmd:///tell %m %!"&sprintf("edit read from %d to %d &quot;%s&quot; page %d'>%d</a> ",{l1,l2,path,i,i})
      end if
    end for
    if p2 < pcount then
      tmp &= "<a href='chatcmd:///tell %m %!"&sprintf("edit read from %d to %d &quot;%s&quot; page %d'>&gt;&gt;</a> ",{l1,l2,path,p2+1})
    end if
    tmp &= "<br>"
  end if
  page -= 1

  line = "\n"
  for i = 1 to length(lines) do
    line &= sprintf("[%d] ",{l1+(page*25)+i-1})&char2xml(txt2xml(lines[i]),'%')
  end for

  if lcount >= 5 then
    run_script(cmd_hsock,cmd_msgID,cmd_groupID,cmd_buddyID
         ,sprintf("edit_read.txt \"%s\" \"%d\" \"%d\" \"%d\" \"%s\" \"%s\"",{path,l1+(page*25),l1+(page*25)+length(lines)-1,lcount,tmp,line}))
  else
    send_msg(line,{})
  end if
  return TRUE
end function

function cmd_edit_write(sequence base, sequence msg)
  return TRUE
end function

function cmd_edit_insert(sequence base, sequence msg)
  return TRUE
end function

function cmd_edit_delete(sequence base, sequence msg)
  return TRUE
end function

function cmd_edit_browse(sequence base, sequence msg)
 sequence path,d,subs,files,out

  msg = get_token(msg," ")
  path = trim_white(strip_quotes(msg[1]))
  if not length(path) then path = "/" end if
  if path[1] != '/' then path = "/"&path end if

  d = dir(base&path)
  if not length(d) then
    send_msg("Invalid path.",{})
    return FALSE
  elsif length(d) = 1 then
    path = reverse(path)
    path = reverse(path[find('/',path)..length(path)])
    d = dir(base&path)
  end if
  if (length(path) > 1) and (path[length(path)] = '/') then path = path[1..length(path)-1] end if

  subs = {}
  files = {}
  for i = 3 to length(d) do
    if find('d',d[i][D_ATTRIBUTES]) then
      subs &={ upper(d[i][D_NAME]) }
    else
      files &={ lower(d[i][D_NAME]) }
    end if
  end for
  subs = sort(subs)
  files = sort(files)

  out = "--&gt; edit browse [<a href=\"text://"
  out &= "<font color=CCInfoText>"
  out &= "<center>"
  out &= "<font color=CCInfoHeadline><u>Edit Browse "&path&"</u></font><br>"
  out &= "<font color=CCInfoHeader>"
  out &= sprintf("%d subdirectories<br>"
                &"%d files<br>",{length(subs),length(files)})
  out &= "</font>"
  out &= "</center>"
  if equal(path,"/") then path = "" end if
  path = upper(path)
  for i = 1 to length(subs) do
    out &= parse_command("  <a href='chatcmd:///tell %m %!edit browse &quot;%1/%2&quot;'>/%2</a><br>",{path,subs[i]})
  end for
  if length(subs) then out &="<br>" end if
  path = lower(path)
  for i = 1 to length(files) do
    out &= parse_command("  <a href='chatcmd:///tell %m %!edit read &quot;%1/%2&quot;'>%2</a><br>",{path,files[i]})
  end for
  out &= "</font>"
  out &= "\">Click Here</a>]"
  send_msg(out,{})

  return TRUE
end function

function cmd_edit_new(sequence base, sequence msg)
  return TRUE
end function

constant EDIT_ARGS = {"read","write","insert","delete","browse","new"}
global constant EDIT_RIDS = {
      routine_id("cmd_edit_read")
     ,routine_id("cmd_edit_write")
     ,routine_id("cmd_edit_insert")
     ,routine_id("cmd_edit_delete")
     ,routine_id("cmd_edit_browse")
     ,routine_id("cmd_edit_new")
}


procedure cmd_edit(sequence args, sequence msg)
 integer mode
trace(ON)
  msg = lower(msg)
  args = get_token(msg," ")
  mode = find(args[1],EDIT_ARGS)
  if mode then msg = args[2] else mode = 1 end if
  if call_func(EDIT_RIDS[mode],{current_dir(),msg}) then return end if
  run_help(cmd_hsock,cmd_msgID,cmd_groupID,cmd_buddyID,{"edit"})
end procedure
new_command("edit",routine_id("cmd_edit"),{})
