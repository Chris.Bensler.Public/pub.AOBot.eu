include std/other/token.e
include api/xml.ew
include etc/wildcard.e

-- with trace

constant MAX_RESULTS = 500

sequence dbID,dbMT,dbQL,dbNAME,dbNAME2

function get_bytes(integer fn, integer n)
 sequence s
  if not n then return {} end if
  s = repeat(EOF,n)
  for i = 1 to n do
    s[i] = getc(fn)
    if s[i] = EOF then return s[1..i-1] end if
  end for
  return s
end function

global procedure load_aodb()
 sequence tmp
 integer fnID,fnMT,fnQL,fnDAT
 atom lp,len,offset

  output("Loading Items DB...",{})
  fnID = fopen("db/item/aoid.idx","rb")
  fnMT = fopen("db/item/meta.idx","rb")
  fnQL = fopen("db/item/ql.idx","rb")
  fnDAT = fopen("db/item/name.idx","rb")
  trap_error(NULL)

  len = fsize(fnID)/4

  dbID = get4u({fnID,len})
  dbMT = get4u({fnMT,len})
  dbQL = get4u({fnQL,len})

  len = fsize(fnDAT)
  lp = alloc(len)
  tmp = get_bytes(fnDAT,4096)
  while length(tmp) do
    poke(lp,tmp)
    lp += length(tmp)
    tmp = get_bytes(fnDAT,4096)
  end while
  lp -= len
  
  dbNAME = repeat(NULL,length(dbID))
  dbNAME2 = dbNAME
  for i = 1 to length(dbNAME) do
    tmp = peek_string(lp)
    lp += length(tmp)+1
    dbNAME[i] = tmp
    tmp *= alphanum_char(tmp)
    tmp += (tmp = 0)*' '
    dbNAME2[i] = lower(trim_white(compress_white(tmp)))
  end for

  dealloc(lp-len)

  fclose(fnID)
  fclose(fnMT)
  fclose(fnQL)
  fclose(fnDAT)
  outputLF("OK\n",{})
end procedure
load_aodb()

constant PREFIX_TABLE = {
      "basic"
     ,"augmented"
     ,"battered"
     ,"enhanced"
     ,"inferior"
     ,"flawless"
     ,"worn"
     ,"high quality"
     ,"overtuned worn"
     ,"overtuned high quality"
     ,"senpai"
     ,"hanshi"
     ,"quality"
     ,"superior"
     ,"treatment"
     ,"hq health"
     ,"superb quality health treatment"
     ,"standard"
     ,"first rate"
     ,"2 3"
     ,"4 7"
     ,"8 15"
     ,"16 31"
     ,"32 63"
     ,"64"
}

function strip_prefix(sequence query)
 sequence tmp
  for i = 1 to length(PREFIX_TABLE) do
    tmp = PREFIX_TABLE[i]&" "
    if (length(query) > length(tmp)) and (match(tmp,query) = 1) then
      return query[length(tmp)+1..length(query)]
    end if
  end for
  return query
end function

function get_results(sequence query)
 sequence ws,s,results,tmp,scores,q
 integer f,offset,count,m
 atom maxscore,max

  ws = get_whitespace()
  set_whitespace({NULL})

  -- split into tokens
  query = lower(query)
  q = query
  q *= alphanum_char(q)
  q = trim_white(compress_white(q))
  set_whitespace(ws)
  if not length(q) then return {0,0,{}} end if

  s = tokenize(q,NULL)

  scores = repeat(NULL,length(dbNAME2))
  max = 0
  for i = 1 to length(s) do
    f = FALSE
    for j = 1 to length(dbNAME2) do
      if scores[j] >= max then
        if match(s[i],dbNAME2[j]) then
          f += 1
          scores[j] += 1
        end if
      end if
    end for
    max += (f != FALSE)
  end for
  
  -- score word order
  for i = 1 to length(scores) do
    tmp = s
    f = 0
    for j = 1 to length(tmp) do
      tmp[j] = match(s[j],dbNAME2[i][f+1..length(dbNAME2[i])])
      if tmp[j] > f then
        f = tmp[j]+length(s[j])
        scores[i] += power(2,length(s)-j+1)
      end if
    end for
  end for

  -- check for exact matches and count results
  count = 0
  maxscore = power(2,length(s)+1) + length(s) + 1
  for i = 1 to length(scores) do
    if scores[i] >= max then
      if scores[i] > max then
        count = 0
        max = scores[i]
      end if
      if equal(query,lower(dbNAME[i])) then
        if max != maxscore then
          count = 0
          max = maxscore
        end if
        scores[i] = max
      end if
      count += 1
    end if
  end for

  results = {}
  if count then
    results = repeat(NULL,count)
    for i = 1 to length(scores) do
      if scores[i] = max then
        results[count] = i
        count -= 1
        if not count then exit end if
      end if
    end for
  end if

  return {maxscore,max,results}
end function

procedure cmd_search(sequence args, sequence msg)
 sequence results,query,out,tmp,tmp2
 integer max,maxscore,pos,count,f,f2,showql
 atom t,idlo,idhi,ql,mt,idx,len,qlmin,qlmax

  if not length(msg) then
    send_msg("You must specify a query phrase to search for.",{})
    return
  end if

  query = trim_white(lower(msg))

  showql = NULL
  if match("ql",query) = 1 then
    query = trim_white(query[3..length(query)])
    tmp = get_token(query," ")
    showql = get_value(tmp[1])
    query = trim_white(tmp[2])
  end if

  t = time()
  results = get_results(query)
  maxscore = results[1]
  max = results[2]
  results = results[3]
  count = length(results)
  out = ""
  out &= sprintf("\nyour query took %dms\n",{(time()-t)*1000})
  if count > MAX_RESULTS*2 then
    out &= "too many matching results. try refining your search query."
  elsif not count then
    out &= "no matching results. try modifying your search query."
  else
    -- remove paired item entries
    tmp = results
    results = {}
    for i = 1 to length(tmp) do
      ql = dbQL[tmp[i]]
      mt = dbMT[tmp[i]]
      if (ql != #FFFFFFFF) and (mt != 'n') then
        f = FALSE
        tmp2 = strip_prefix(dbNAME2[tmp[i]])
        for j = 1 to length(results) do
          for k = 1 to length(results[j][1]) do
            if equal(tmp2,strip_prefix(dbNAME2[results[j][1][k]])) then
              f = j
              exit
            end if
          end for
        end for
        if f then
          f2 = FALSE
          for k = 1 to length(results[f][2]) do
            if ql < results[f][2][k] then
              f2 = k
              exit
            end if
          end for
          if f2 then
            results[f] = vsplice(results[f],{f2,f2-1},{tmp[i],ql})
          else
            results[f] = vappend(results[f],{tmp[i],ql})
          end if
        else
          results &={ {{tmp[i]},{ql}} }
        end if
      end if
    end for

    -- filter ql
    if showql then
      tmp = results
      results = {}
      for i = length(tmp) to 1 by -1 do
        if (showql >= tmp[i][2][1]) and (showql <= tmp[i][2][length(tmp[i][2])]) then
          results &={ tmp[i] }
          tmp = splice(tmp,i,{})
        end if
      end for
      if length(results) then results = reverse(results) end if
      results &= tmp
    end if

    out &= sprintf("\nyour query took %dms\n",{(time()-t)*1000})
    out &= sprintf("%d results scored %.1f%%\n",{length(results),round_places(max/maxscore * 100,-1)})

    -- show the top 3
    for i = 1 to length(results) do
      if i > 3 then exit end if
      len = length(results[i][1])
      mt = dbMT[results[i][1][len]]
      idlo = dbID[results[i][1][1]]
      idhi = idlo
      idx = results[i][1][len]
      qlmin = results[i][2][1]
      qlmax = results[i][2][len]
      ql = qlmax
      if in_range(showql,qlmin,qlmax) then ql = showql else showql = NULL end if
      if len > 1 then
        for j = 1 to len-1 do
          if in_range(ql,results[i][2][j],results[i][2][j+1]) then
            idlo = dbID[results[i][1][j]]
            idhi = dbID[results[i][1][j+1]]
            idx = results[i][1][j]
            qlmin = results[i][2][j]
            qlmax = results[i][2][j+1]
            exit
          end if
        end for
      end if
      out &= sprintf("%d. <a href='itemref://%d/%d/%d'>%s</a>  QL %d (%d to %d) [%s]\n",{i,idlo,idhi,ql,dbNAME[idx],ql,qlmin,qlmax,mt})
    end for
    if length(results) > 3 then
      out &= "more... <a href=\"text://"
      out &= "<center><font color=CCInfoHeadline><u>Extended Search Results</u></font></center><br><br>"
      for i = 1 to length(results) do
        if i > MAX_RESULTS then exit end if
        len = length(results[i][1])
        mt = dbMT[results[i][1][len]]
        if len > 1 then
          idlo = dbID[results[i][1][len-1]]
        end if
        idhi = dbID[results[i][1][len]]
        if len = 1 then idlo = idhi end if
        qlmin = results[i][2][1]
        qlmax = results[i][2][len]
        ql = qlmax
        if (showql >= qlmin) and (showql <= qlmax) then ql = showql end if
        out &= sprintf("%d. <a href='itemref://%d/%d/%d'>%s</a>  QL %d (%d to %d) [%s]\n",{i,idlo,idhi,ql,dbNAME[results[i][1][len]],ql,qlmin,qlmax,mt})
      end for
      out &= "\">here</a>"
    end if
  end if
  send_msg(out,{})
end procedure
new_command("search",routine_id("cmd_search"),{})