include standard.em
-- with trace
-- trace(1)

global function qwild_match(sequence pattern,sequence s)
-- 'q'uestion wild match
-- only supports ? wildcards
 integer m,pl,sl,si,pf,pc

  if length(pattern) > length(s) then return FALSE end if
  pf = find('?',pattern)
  if not pf then
    return match(pattern,s)
  elsif (pf = 1) then
    for i = 1 to length(pattern) do -- get the first non-? char in the pattern
      if (pattern[i] != '?') then
        pf = i
        exit
      end if
    end for
    if (pf=1) then return (length(pattern) <= length(s)) end if -- all ?'s
  else
    pf = 1
  end if
  pc = pattern[pf]
  m = find(pc,s[pf..length(s)]) -- don't have to bother to check the chars before the first possible match of the first char
  if not m then return FALSE end if -- couldn't find the first char in the pattern, no match

  m += pf-1
  pl = length(pattern)
  sl = length(s)-pl+pf
  si = m
  while (m <= sl) do
    for pi = pf to pl do
      if (s[si] != pattern[pi]) then
        if (pattern[pi] != '?') then
          si = FALSE
          exit -- pi
        end if
      end if
      si +=1
    end for
    if si then return m-pf+1 end if

    for f = m+1 to sl do
      if (s[f] = pc) then
        si = f
        m = si
        exit
      end if
    end for
    if not si then return FALSE end if
  end while

  return FALSE
end function

global function awild_match(sequence p, sequence s)
-- 'a'sterisk wild match
-- only supports * wildcards
 integer m,ps,pp,sp,sl
  p &= EOF
  sl = length(s)
  pp = 1 -- pattern position
  sp = 1 -- string position
  while (sp <= sl) do
    if (p[pp] = '*') then -- * ,match next pattern token
      while (p[pp] ='*') do -- skip consecutive wildcards, redundant
        pp +=1
      end while
      if p[pp] = EOF then return TRUE end if
      -- find the next wildcard
      ps = pp
      while (p[pp] != '*') and (p[pp] != EOF) do
        pp +=1
      end while
      if (p[pp] = EOF) then
        sp = ( sl - (pp-ps) + 1 )
      end if
      m = match(p[ps..pp-1],s[sp..sl])
      if not m then return FALSE end if
      sp = sp + m + (pp-ps) - 1
    elsif (p[pp] = s[sp]) then
      pp +=1      sp +=1
    else
      return (p[pp] = EOF)
    end if
  end while  
  return TRUE
end function

global function wild_match(sequence p, sequence s)
-- general wild match
-- supports both * and ? wildcards
 integer m,ps,pp,sp,sl
  p &= EOF
  sl = length(s)
  pp = 1 -- pattern position
  sp = 1 -- string position
  while (sp <= sl) do
    if (p[pp] = '?') then
      pp +=1      sp +=1
    elsif (p[pp] = '*') then -- * ,match next pattern token
      while (p[pp] ='*') do -- skip consecutive wildcards, redundant
        pp +=1
      end while
      if p[pp] = EOF then return TRUE end if
      -- find the next wildcard
      ps = pp
      while (p[pp] != '*') and (p[pp] != EOF) do
        pp +=1
      end while
      if (p[pp] = EOF) then
        sp = ( sl - (pp-ps) + 1 )
      end if
      m = qwild_match(p[ps..pp-1],s[sp..sl])
      if not m then return FALSE end if
      sp = sp + m + (pp-ps) - 1
    elsif (p[pp] = s[sp]) then
      pp +=1      sp +=1
    else
      return (p[pp] = EOF)
    end if
  end while  
  return TRUE
end function
