global constant GMP_DLL = open_dll("dll/gmp.dll")

global constant C__mpz_struct = define_struct({
    {"_mp_alloc",INT} -- Number of *limbs* allocated and pointed to by the _mp_d field.
   ,{"_mp_size",INT}  -- abs(_mp_size) is the number of limbs the last field points to.
                      -- If _mp_size is negative this is a negative number.
   ,{"_mp_d",PTR}     -- mp_limb_t;		/* Pointer to the limbs.  */
})


global constant C__gmpz_init = define_c_proc(GMP_DLL,"__gmpz_init",{PTR})
global function mpz_init()
 atom lpdst
  lpdst = alloc(sizeOf(C__mpz_struct))
  c_proc(C__gmpz_init,{lpdst})
  return lpdst
end function

global constant C__gmpz_init_set_str = define_c_func(GMP_DLL,"__gmpz_init_set_str",{PTR, PTR, INT},INT)
global function mpz_init_set_str(atom lpdst, object s, integer base)
 atom lpsz
  if not lpdst then lpdst = alloc(sizeOf(C__mpz_struct)) end if
  if sequence(s) then lpsz = alloc_sz(s) else lpsz = s end if

  if c_func(C__gmpz_init_set_str,{lpdst,lpsz, base}) then
    error("mpz_init_set_str(): base does not match string type")
  end if

  if sequence(s) then dealloc(lpsz) end if
  return lpdst
end function

global constant C__gmpz_get_str = define_c_func(GMP_DLL,"__gmpz_get_str",{PTR, INT, PTR},PTR)
global function mpz_get_str(integer base, atom lpsrc)
 sequence ret
 atom lpdst
  lpdst = alloc(#FFFF)
  lpdst = c_func(C__gmpz_get_str,{lpdst,base,lpsrc})
  ret = peek_string(lpdst)
  dealloc(lpdst)
  return ret
end function

global constant C__gmpz_powm = define_c_proc(GMP_DLL,"__gmpz_powm",{PTR, PTR, PTR, PTR})
global function mpz_powm(atom lpdst, object mpz1, object mpz2, object mpz3)
 atom lp1,lp2,lp3
  lp1 = NULL    lp2 = NULL    lp3 = NULL
  if not lpdst then lpdst = mpz_init() end if
  if sequence(mpz1) then lp1 = mpz_init_set_str(lp1,mpz1,16) else lp1 = mpz1 end if
  if sequence(mpz2) then lp2 = mpz_init_set_str(lp2,mpz2,16) else lp2 = mpz2 end if
  if sequence(mpz3) then lp3 = mpz_init_set_str(lp3,mpz3,16) else lp3 = mpz3 end if

  c_proc(C__gmpz_powm,{lpdst,lp1,lp2,lp3})

  if sequence(mpz1) then dealloc(lp1) end if
  if sequence(mpz2) then dealloc(lp2) end if
  if sequence(mpz3) then dealloc(lp3) end if

  return lpdst
end function

global function powm(sequence x, sequence y, sequence z)
 sequence ret
 atom lpdst
  lpdst = mpz_powm(NULL,x, y, z)
  ret = mpz_get_str(16,lpdst)
  dealloc(lpdst)
  return ret
end function
